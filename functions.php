<?php
    // Menu Registration
    register_nav_menus(array(
        'primary_nav' => 'Main Header Navigation',
        'footer_nav' => 'Footer Navigation'
    ));

    // Side bar Registration
    add_action( 'widgets_init', 'regsiter_theme_sidebars' );
    function regsiter_theme_sidebars() {
        register_sidebar(
            array(
                'id' => 'primary',
                'name' => __( 'Primary' ),
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget' => '</div>',
                'before_title' => '<h3 class="widget-title">',
                'after_title' => '</h3>'
            )
        );
    }

	
    
    // Misc registrations
    add_theme_support('post-thumbnails');
    add_image_size("product-thumb", 300, 300, TRUE);
    add_image_size("slider-image", 1200, 400, TRUE);
    add_image_size("slider-pager", 75, 75, TRUE);

    function slideimage_column_head_only_slide($defaults) {
        $defaults['slide_image'] = 'Slide Image';
        return $defaults;
    }     
    function slideimage_column_content_only_slide($column_name, $post_ID) {
        if ($column_name == 'slide_image') {
            $image = get_post_meta($post_ID, "image", TRUE);
            $image = wp_get_attachment_image_src($image['ID'], "slider-pager");
            $post_featured_image = $image[0];
            if ($post_featured_image) {
                echo '<img src="' . $post_featured_image . '" />';
            }
        }
    }
    add_filter('manage_slide_posts_columns', 'slideimage_column_head_only_slide');
    add_action('manage_slide_posts_custom_column', 'slideimage_column_content_only_slide', 10, 2);

	function ShortenText($text) {
        $chars_limit = 25;
        $chars_text = strlen($text);
        $text .= " ";
        $text = substr($text,0,$chars_limit);
        $text = substr($text,0,strrpos($text,' '));

        if ($chars_text > $chars_limit)
        { $text = $text."..."; }
        return $text;
	}
    function get_theme_part($folder, $file) {
        $url = get_template_directory_uri() . "/";
        if($folder !== null) $url .= $folder . "/";
        $url .= $file;
        return $url;
    }
    function related_products($id) {
        $the_terms = get_the_terms($id, "product_category");
        if(count($the_terms) > 1) {
            foreach ($the_terms as $term) {
                if($term->term_id !== 10) {
                    $the_term = $term->slug;
                }
            }
        } else {
            $the_term = $the_terms->slug;
        }
        $args = array(
            "post_type"         => "product",
            "product_category"  => $the_term,
            "post__not_in"      => array($id),
            "posts_per_page"    => 5

        );
        $the_query = new WP_Query($args);
        if($the_query->have_posts()) :
        echo '<ul class="related-products">';
        while($the_query->have_posts()) : $the_query->the_post();
        echo '<li><a href="'.get_permalink().'">';
            echo '<div class="uk-display-block uk-width-1-1">';
                the_post_thumbnail(array(75,75),array("class"=>"uk-align-left"));
                the_title();
                echo '<span class="uk-display-block uk-margin-remove">';
                $inv = get_post_meta(get_the_ID(), "mp_inventory", true);
                if($inv[0] >= 1) {
                    mp_product_price(true, get_the_ID(), "");
                } else {
                    echo "Sold";
                }
                echo '</span>';
            echo '</div> <div class="uk-clearfix"></div>';
        echo '</a></li>';
        endwhile; wp_reset_postdata();  
        echo '</ul>';
        else : ?>
            <div class="uk-text-left order-bespoke">Sorry we couldn't find any other pieces for you..<br /><a href="<?php echo get_permalink(60); ?>">Why not order a Bespoke piece</a>.</div>
        <?php 
        endif;
    }
    
    class Walker_UIKIT extends Walker {

        public $has_children = false;
        /**
         * What the class handles.
         *
         * @see Walker::$tree_type
         * @since 3.0.0
         * @var string
         */
        var $tree_type = array(
            'post_type',
            'taxonomy',
            'custom'
        );
        /**
         * Database fields to use.
         *
         * @see Walker::$db_fields
         * @since 3.0.0
         * @todo Decouple this.
         * @var array
         */
        var $db_fields = array(
            'parent' => 'menu_item_parent',
            'id' => 'db_id'
        );
        /**
         * Starts the list before the elements are added.
         *
         * @see Walker::start_lvl()
         *
         * @since 3.0.0
         *
         * @param string $output Passed by reference. Used to append additional content.
         * @param int    $depth  Depth of menu item. Used for padding.
         * @param array  $args   An array of arguments. @see wp_nav_menu()
         */
        function start_lvl(&$output, $depth = 0, $args = array()) {
            $indent = str_repeat("\t", $depth);
            
            if ($this->has_children && $depth == 0) {

                $output.= "$indent<div class=\"uk-dropdown uk-dropdown-navbar uk-dropdown-width-1\">\n";
                //   $output.= "$indent<div class=\"uk-grid\">\n";
                //   $output.= "$indent<div class=\"uk-width-1-1\">\n";
                $indent = "\t$indent";
                $output.= "\n$indent<ul class=\"uk-nav uk-nav-navbar\">\n";
            } else {
                $output.= "\n$indent<ul class=\"uk-nav-sub\">\n";
            }
        }
        /**
         * Ends the list of after the elements are added.
         *
         * @see Walker::end_lvl()
         *
         * @since 3.0.0
         *
         * @param string $output Passed by reference. Used to append additional content.
         * @param int    $depth  Depth of menu item. Used for padding.
         * @param array  $args   An array of arguments. @see wp_nav_menu()
         */
        function end_lvl(&$output, $depth = 0, $args = array()) {
            $indent = str_repeat("\t", $depth);
            $output.= "$indent</ul>\n";
                 
        }
        /**
         * Start the element output.
         *
         * @see Walker::start_el()
         *
         * @since 3.0.0
         *
         * @param string $output Passed by reference. Used to append additional content.
         * @param object $item   Menu item data object.
         * @param int    $depth  Depth of menu item. Used for padding.
         * @param array  $args   An array of arguments. @see wp_nav_menu()
         * @param int    $id     Current item ID.
         */
       
      
        function start_el(&$output, $item, $depth = 0, $args = array() , $id = 0) {
        
            $indent = ($depth) ? str_repeat("\t", $depth) : '';
            $class_names = $value = '';
            $classes = empty($item->classes) ? array() : (array)$item->classes;
            $classes[] = 'menu-item-' . $item->ID;
            /**
             * Filter the CSS class(es) applied to a menu item's <li>.
             *
             * @since 3.0.0
             *
             * @param array  $classes The CSS classes that are applied to the menu item's <li>.
             * @param object $item    The current menu item.
             * @param array  $args    An array of arguments. @see wp_nav_menu()
             */
            $class_names = join(' ', apply_filters('nav_menu_css_class', array_filter($classes) , $item, $args));
            $this->has_children = in_array('menu-item-has-children', $classes);
            
            if (in_array('current-menu-item', $classes)) {

                $class_names.= ' uk-active';
            }
            
            if ($this->has_children) {

                $class_names.= ' uk-parent';
            }
            
            if (in_array('current-menu-ancestor', $classes)) {

                $class_names.= ' uk-active';
            }
            $class_names = $class_names ? ' class="' . esc_attr($class_names) . '"' : '';
            /**
             * Filter the ID applied to a menu item's <li>.
             *
             * @since 3.0.1
             *
             * @param string The ID that is applied to the menu item's <li>.
             * @param object $item The current menu item.
             * @param array $args An array of arguments. @see wp_nav_menu()
             */
            $id = apply_filters('nav_menu_item_id', 'menu-item-' . $item->ID, $item, $args);
            $id = $id ? ' id="' . esc_attr($id) . '"' : '';
            $data_dropdown = '';
            
            if ($this->has_children && $depth == 0) {

                $data_dropdown = ' data-uk-dropdown';
            }
            $output.= $indent . '<li' . $id . $value . $class_names . $data_dropdown . '>';
            $atts = array();
            $atts['title'] = !empty($item->attr_title) ? $item->attr_title : '';
            $atts['target'] = !empty($item->target) ? $item->target : '';
            $atts['rel'] = !empty($item->xfn) ? $item->xfn : '';
            $atts['href'] = !empty($item->url) ? $item->url : '';
            /**
             * Filter the HTML attributes applied to a menu item's <a>.
             *
             * @since 3.6.0
             *
             * @param array $atts {
             *     The HTML attributes applied to the menu item's <a>, empty strings are ignored.
             *
             *     @type string $title  The title attribute.
             *     @type string $target The target attribute.
             *     @type string $rel    The rel attribute.
             *     @type string $href   The href attribute.
             * }
             * @param object $item The current menu item.
             * @param array  $args An array of arguments. @see wp_nav_menu()
             */
            $atts = apply_filters('nav_menu_link_attributes', $atts, $item, $args);
            $attributes = '';
            
            foreach ($atts as $attr => $value) {

                
                if (!empty($value)) {

                    $value = ('href' === $attr) ? esc_url($value) : esc_attr($value);
                    $attributes.= ' ' . $attr . '="' . $value . '"';
                }
            }
           
            $item_output = $args->before;
            $item_output .= '<a'. $attributes .'>';
            /** This filter is documented in wp-includes/post-template.php */
            $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
            $item_output .= '</a>';
            $item_output .= $args->after;
           
            /**
             * Filter a menu item's starting output.
             *
             * The menu item's starting output only includes $args->before, the opening <a>,
             * the menu item's title, the closing </a>, and $args->after. Currently, there is
             * no filter for modifying the opening and closing <li> for a menu item.
             *
             * @since 3.0.0
             *
             * @param string $item_output The menu item's starting HTML output.
             * @param object $item        Menu item data object.
             * @param int    $depth       Depth of menu item. Used for padding.
             * @param array  $args        An array of arguments. @see wp_nav_menu()
             */
           
            $output.= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
        }
        /**
         * Ends the element output, if needed.
         *
         * @see Walker::end_el()
         *
         * @since 3.0.0
         *
         * @param string $output Passed by reference. Used to append additional content.
         * @param object $item   Page data object. Not used.
         * @param int    $depth  Depth of page. Not Used.
         * @param array  $args   An array of arguments. @see wp_nav_menu()
         */
        function end_el(&$output, $item, $depth = 0, $args = array()) {
            $classes = empty($item->classes) ? array() : (array)$item->classes;
            $has_children = in_array('menu-item-has-children', $classes);
            if ($has_children && $depth == 0) {
                
                $output.= "</div>\n";
            }
            $output.= "</li>\n";
        }
        public static function fallback( $args ) {
            if ( current_user_can( 'manage_options' ) ) {

                extract( $args );

                $fb_output = null;

                if ( $container ) {
                    $fb_output = '<' . $container;

                    if ( $container_id )
                        $fb_output .= ' id="' . $container_id . '"';

                    if ( $container_class )
                        $fb_output .= ' class="' . $container_class . '"';

                    $fb_output .= '>';
                }

                $fb_output .= '<ul';

                if ( $menu_id )
                    $fb_output .= ' id="' . $menu_id . '"';

                if ( $menu_class )
                    $fb_output .= ' class="' . $menu_class . '"';

                $fb_output .= '>';
                $fb_output .= '<li><a href="' . admin_url( 'nav-menus.php' ) . '">Add a menu</a></li>';
                $fb_output .= '</ul>';

                if ( $container )
                    $fb_output .= '</' . $container . '>';

                echo $fb_output;
            }
        }
    } // Walker_Nav_Menu
