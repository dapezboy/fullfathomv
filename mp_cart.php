<?php
	get_header();
?>
	<div class="uk-container uk-container-center checkout">
		<div class="uk-grid uk-margin-top">
			<div class="uk-width-large-7-10">
			<?php while (have_posts()) : the_post(); ?>
				<article class="uk-article">
					<h2 class="uk-article-title dark-pink">Checkout</h2>
					<?php mp_show_cart('checkout'); ?>
				</article>
			<?php endwhile; ?>
			</div>
			<div class="uk-width-3-10 uk-visible-large">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
<?php
	get_footer();