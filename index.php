<?php
	get_header();
?>
	<div class="uk-width-1-1 uk-margin-small-bottom">
		<ul class="bxslider">
			<?php
				$slider = array(
					"post_type"			=> "slide",
					"posts_per_page"	=> -1,
				);
				query_posts($slider);
				while (have_posts()) : the_post();
				$image = get_post_meta($post->ID, "image", TRUE);
				$images[] = $image["ID"];
			?>
				<li><?php echo wp_get_attachment_image($image["ID"], "slider-image", false, array("class" => "uk-responsive-height uk-align-center uk-margin-bottom-remove")); ?></li>
			<?php endwhile; wp_reset_query(); ?>
		</ul>
		<div id="bx-pager" class="uk-container uk-container-center uk-text-center uk-hidden-small uk-margin-top-remove">
		<?php $count = 0; foreach ($images as $image) : ?>
			  <a data-slide-index="<?php echo $count; ?>" href=""><?php echo wp_get_attachment_image($image, "slider-pager", false, array("class" => "uk-thumbnail")); ?></a>
		<?php $count++; endforeach; ?>
		</div>
		<script>
			$(document).ready(function() {
				$('.bxslider').bxSlider({
					mode: 'fade',
					auto: true,
					autoHover: true,
					useCSS: false,
					controls: false,
					pagerCustom: '#bx-pager'
				});
			});
		</script>
	</div>
<div class="uk-container uk-container-center uk-margin-large-top">
	<div class="uk-width-1-1">
		<h2 class="fav-products uk-text-center">Featured Treasures</h2>
	</div>
	<ul class="uk-grid fav-products" data-uk-grid-margin data-uk-grid-match="{target:'.uk-panel', row: true}">
	<?php
		$args = array(
			'post_type' => 'product',
			'product_category' => 'featured',
			'orderby' => 'rand',
			'posts_per_page' => 8
		);
		$wp_query = new WP_Query($args);
		if ( have_posts() ) : while ( $wp_query->have_posts() ) : $wp_query->the_post();
	?>
		<li class="uk-width-medium-1-4">
			<?php get_template_part("product","loop_other"); ?>
		</li>
	<?php endwhile; endif; wp_reset_query(); ?>
	</ul>
</div>
<?php
	get_footer();