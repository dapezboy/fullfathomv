<?php
	get_header();
?>
	<div class="uk-container uk-container-center single">
		<div class="uk-grid uk-margin-top">
			<div class="uk-width-large-7-10">
			<?php while (have_posts()) : the_post(); ?>
				<article class="uk-article">
					<h2 class="uk-article-title uk-margin-bottom-remove"><a class="no-deco" href="<?php the_permalink() ;?>"><?php the_title(); ?></a></h2>
					<div class="uk-article-meta uk-margin-bottom">Posted on <?php the_date(); ?> by <?php the_author(); ?> in <?php the_category(", "); ?></div>
					<?php the_content(); ?>
				</article>
			<?php endwhile; ?>
			</div>
			<div class="uk-width-3-10 uk-visible-large">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
<?php
	get_footer();