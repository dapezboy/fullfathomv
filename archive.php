<?php get_header(); ?>
	<div class="uk-container uk-container-center">
		<div class="uk-grid uk-margin-top">
			<div class="uk-width-large-7-10 headerpush">
			<?php
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				$args = array(
					"posts_per_page"	=> get_option("posts_per_page"),
					"paged"				=> $paged
				);
				query_posts($args);
				while (have_posts()) : the_post();
			?>
				<article class="uk-article">
					<h2 class="uk-article-title uk-margin-bottom-remove"><a class="no-deco" href="<?php the_permalink() ;?>"><?php the_title(); ?></a></h2>
					<div class="uk-article-meta uk-margin-bottom"><?php the_time( get_option( 'date_format' ) ); ?> by: <?php the_author(); ?> in <?php the_category(", "); ?></div>
					<?php the_excerpt(); ?>
					<?php endif; ?>
				</article>
				<?php endwhile; ?>
			</div>
			<div class="uk-width-3-10 uk-visible-large">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
<?php get_footer(); ?>