<footer class="uk-width-1-1 uk-margin-large-top">
	<div class="links">
		<div class="uk-text-center uk-text-small">
			<ul>
			<li><a href="<?php echo get_permalink(10); ?>">Contact Us</a></li>
			<li><a href="<?php echo get_permalink(707); ?>" >Privacy Policy</a></li>
			<li><a href="<?php echo get_permalink(705); ?>">Terms of Use</a></li>
			</ul>
		</div>
	</div>
	<div class="ak">
		<div class="uk-text-center uk-text-small">
			&copy; 2014 <?php echo get_bloginfo( "name", "raw"); ?>. All Rights Reserved. &bull; <a href="http://ankemp.com/">Design &amp; Development by Andrew Kemp</a>
		</div>
	</div>
</footer>
<?php wp_footer(); ?>

</body>
</html>