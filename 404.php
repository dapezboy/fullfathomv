<?php get_header(); ?>

<div class="uk-container uk-container-center 404">
	<h1 class="uk-text-center fourohfour">404</h1>
	<div class="uk-text-center uk-text-large">Yikes! The page you're looking for cannot be found!</div>
</div>

<?php get_footer(); ?>