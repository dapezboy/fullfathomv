<?php
/*
	Template Name: Full-Width
*/
	get_header();
?>
	<div class="uk-container uk-container-center">
		<div class="uk-width-8-10 uk-container-center">
		<?php while (have_posts()) : the_post(); ?>
			<article class="uk-article">
				<h2 class="uk-article-title"><?php the_title(); ?></h2>
				<?php the_content(); ?>
			</article>
		<?php endwhile; ?>
		</div>
	</div>
<?php
	get_footer();