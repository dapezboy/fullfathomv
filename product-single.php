<article class="uk-article product-page">
	<h1 class="uk-article-title uk-margin-remove"><?php the_title(); ?></h1>
	<div class="uk-article-meta">in <span><?php echo mp_category_list($post->ID); ?></span></div>
	<div class="uk-grid uk-margin-top">
		<div class="uk-width-medium-1-2">
			<div class="swap">
				<?php the_post_thumbnail(); ?>
			</div>
		</div>
		<div class="uk-width-medium-1-2 uk-text-center">
			<div class="uk-display-block uk-width-1-1 uk-margin-large-top uk-margin-bottom price"><?php mp_product_price(true, $post->ID, ""); ?></div>
			<div class="uk-display-block uk-width-1-1 addtocart"><?php mp_buy_button(true, 'single'); ?></div>
			<div class="uk-margin-top uk-text-large order-bespoke" style="display: none;">
				Not finding the piece you're looking for?<br /><a href="<?php echo get_permalink(60); ?>">Order a Bespoke piece</a>.
			</div>
		</div>
		<div class="uk-width-1-1 uk-margin-top"></div>
		<div class="uk-width-medium-1-2 more-images">
			<div class="uk-grid" data-uk-grid-margin>
			<?php
				foreach(get_post_meta($post->ID, "other_images") as $image) : 
				$image_attr = wp_get_attachment_image_src($image["ID"], "thumbnail");
				$image_lrg = wp_get_attachment_image_src($image["ID"], "product-thumb")
			?>
				<img src="<?php echo $image_attr[0];  ?>" alt="<?php echo $image["post_title"]; ?>" data-full="<?php echo $image_lrg[0]; ?>" class="uk-width-1-2" />
			<?php endforeach; ?>
			<script>
				$(window).load(function() {
					$('.swap').css('height', $('.swap').height());
					var defimg;
					$('.more-images img').hover(function() {
						defimg = $('.swap img').attr('src');
						$('.swap img').attr('src', $(this).attr('data-full'));
					}, function() {
						$('.swap img').attr('src', defimg);
					});
					var view_count = $.cookie('view_count');
					if(isNaN(view_count)) {
						view_count = 1;
					} else {
						view_count = parseInt(view_count);
					}
					if(view_count > 3) {
						$('.order-bespoke').delay(1000).fadeIn('slow');
					}
					view_count += 1;
					$.cookie('view_count', view_count, { expires: 1, path: '/' });
				});
			</script>
			</div>
		</div>
		<div class="uk-width-medium-1-2 content">
			<?php mp_product_description($post->ID); ?>
			<p>Each piece of sea glass in our collection was found by us on the beaches of the North Shore, and with some of our more remarkable finds, we can remember exactly where and when we found them. I have sorted, graded and considered each piece, at length, before the jewelry process begins and they are named when finished. We remember them all, and sometimes, we're lucky enough to meet the person taking them on their new journey and we remember them too. When you take possession of a Full Fathom Five jewelry piece you also take with you a promise, that no matter how much time goes by, or how hard you play in your FF5, as long as I have hands, I will service and restore your sea glass jewelry.</p>
			<p>The performance of the manmade materials chosen for the Full Fathom Five jewelry line have been tested, and we are generally pleased. But if you wear them everyday like we do, play hard in the ocean, or just play hard (stories are encouraged), then your FF5 jewelry will need a spa day eventually. When that day comes, I insist that you allow me to restore the piece to it's original condition. You and the sea glass deserve it. The only cost will be postage. You pay postage to me and I'll pay it back to you.</p>
			<p>Additionally, we would also like to offer an upgrade option should you want to use your sea glass in new and unusual ways. My creative process is dynamic, always evolving and sometimes it's your wishes that spark this process even more. By and through these challenges,the flow, expression and technical skills in the work are constantly being refined and improved. Please feel free to start a conversation about your sea glass dreams.</p>
			<p>Thank you for your enthusiasm and support for Full Fathom Five.</p>
			<p>We are grateful.</p>
		</div>
	</div>
</article>