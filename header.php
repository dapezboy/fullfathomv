<!DOCTYPE html>
<html>
    <head>
    	<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <title><?php wp_title( '|', true, 'right' ); ?><?php echo get_bloginfo( "name", "raw"); ?></title>

		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

        <link href="<?php echo get_theme_part("css", "base.css"); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo get_theme_part("css", "addons.css"); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo get_theme_part("css", "bxslider.css"); ?>" rel="stylesheet" type="text/css" />
        
       
	 	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="<?php echo get_theme_part("js", "base.js"); ?>"></script>
        <script src="<?php echo get_theme_part("js/addons", "jquery.fitvids.js"); ?>"></script>
        <script src="<?php echo get_theme_part("js/addons", "jquery.bxslider.js"); ?>"></script>
        <script src="<?php echo get_theme_part("js/addons", "jquery.cookie.js"); ?>"></script>

        <link rel="shortcut icon" href="/favicon.ico">
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
        <?php wp_head(); ?>
        <link href="<?php echo get_theme_part(null, "style.css"); ?>" rel="stylesheet" type="text/css" />
        <script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-55652656-1', 'auto');
			ga('send', 'pageview');
		</script>
    </head>
    <body <?php body_class(); ?>>
    <div id="fb-root"></div>
	<script>
	(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=1536243836598492&version=v2.0";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script>
	<script>(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];g.src="//instagramfollowbutton.com/components/instagram/v2/js/ig-follow.js";s.parentNode.insertBefore(g,s);}(document,"script"));</script>
		<?php
	        $menuargs = array(
	            "container"         => "",
	            "theme_location"    => "primary_nav",
	            "menu_class"        => "uk-navbar-nav uk-visible-large",
	            "menu_id"           => "",
	            'walker'            => new Walker_UIKIT
            );
            $offcanvnav = array(
	            "container"         => "",
	            "theme_location"    => "primary_nav",
	            "menu_class"        => "uk-nav uk-nav-offcanvas",
	            "menu_id"           => ""
        	);
        ?>
    <header class="uk-width-1-1">
	    <div id="offcanv" class="uk-offcanvas">
			<div class="uk-offcanvas-bar uk-offcanvas-bar-flip">
				<div class="uk-panel">
					<div class="fb-like" data-href="https://www.facebook.com/pages/Full-Fathom-Five/388223577854810" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
					<span class="ig-follow" data-id="7a7c1b3dd2" data-count="true" data-size="small" data-username="false"></span>
					<a href="https://plus.google.com/112283065466325725917?prsrc=3" rel="publisher" target="_top" style="text-decoration:none;">
						<img src="//ssl.gstatic.com/images/icons/gplus-16.png" alt="Google+" style="border:0;width:16px;height:16px;"/>
					</a>
				</div>
				<?php wp_nav_menu($offcanvnav); ?>
			</div>
		</div>
		<div class="uk-container uk-container-center">
		<nav>
			<div class="logo-box">
				<a href="<?php echo home_url(); ?>">
					<img src="<?php echo get_theme_part("images", "logo.jpg"); ?>" alt="<?php echo get_bloginfo( "name", "raw"); ?>" class="logo" />
					<h1>Full Fathom Five Hawaii</h1>
				</a>
			</div>
			<div class="uk-float-right uk-margin-top">
				<?php wp_nav_menu($menuargs); ?>
			</div>
			<div class="uk-clearfix"></div>
		</nav>
		</div>
		<nav class="uk-navbar sepp uk-navbar-attached">
			<div class="uk-container uk-container-center">
				<div class="uk-navbar-brand uk-hidden-small">
					<div class="fb-like" data-href="https://www.facebook.com/pages/Full-Fathom-Five/388223577854810" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
					<span class="ig-follow" data-id="7a7c1b3dd2" data-count="true" data-size="small" data-username="false"></span>
					<a href="https://plus.google.com/112283065466325725917?prsrc=3" rel="publisher" target="_top" style="text-decoration:none;">
						<img src="//ssl.gstatic.com/images/icons/gplus-16.png" alt="Google+" style="border:0;width:16px;height:16px;"/>
					</a>
				</div>
				<div class="uk-navbar-center uk-hidden-small">
					<?php echo get_bloginfo('description', 'raw'); ?>
				</div>
				<div class="cart">
					<?php echo do_shortcode( '[mpawc design="link" cartstyle="dropdown-click" onlystorepages="no" btncolor="grey"]' ); ?>
				</div>
			</div>
			<div class="uk-clearfix"></div>
		</nav>
		<a href="#offcanv" class="uk-navbar-toggle uk-float-right uk-margin-right uk-hidden-large" data-uk-offcanvas></a>
		<div class="uk-clearfix"></div>
    </header>